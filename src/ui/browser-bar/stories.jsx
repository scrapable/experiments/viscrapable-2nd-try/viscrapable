import React from "react"
import BrowserBar from "./index.jsx"
import "../style.css"

export default {
	title: "BrowserBar",
}

export const withLocation = () => (
	<div style={{background: "var(--bg-shade-1)", height: "2rem"}}>
		<BrowserBar location="https://duck.com?q=queryparam1234#hash" />
	</div>
)
