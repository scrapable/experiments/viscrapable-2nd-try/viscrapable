import React from "react"
import ScreenNavButtons from "./index.jsx"
import "../style.css"

export default {
	title: "ScreenNavButtons",
}

const FakeLink = props => <div {...props} href={props.to} />

const Wrapper = props => (
	<div style={{ padding: "1rem" }}>
		<ScreenNavButtons {...props} Link={FakeLink} />
	</div>
)

export const defaultWithPrevAndNext = () => (
	<Wrapper previous="#" next="#" />
)

export const justWithNext = () => (
	<Wrapper next="#" />
)

export const justWithPrev = () => (
	<Wrapper previous="#" />
)

export const withPrevLabel = () => (
	<Wrapper previous="#" previousLabel="Previous" />
)

export const withCustomLabels = () => (
	<Wrapper previous="#" next="#" nextLabel="Forward" previousLabel="Back" />
)
