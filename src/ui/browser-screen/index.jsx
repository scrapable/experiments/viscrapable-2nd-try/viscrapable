import React, { Component } from "react"
import { hot } from "react-hot-loader/root"
import "./style.css"


class BrowserScreen extends Component {
	constructor(props) {
		super(props)
		this.screenImgRef = React.createRef()
	}

	render() {
		const {
			tabIndex,
			screenshot,
			onEvent,
		} = this.props


		const dispatchEvent = async event => {
			if(onEvent)
				onEvent(event)
		}

		let x, y
		let lastX = 0, lastY = 0

		const dispatchMouseMoveEvent = async () => {
			await dispatchEvent({
				op: "mouseMove",
				args: { x, y },
			})
			lastX = x
			lastY = y
		}

		const handleMouseMove = async evt => {
			x = Math.floor(((evt.pageX - this.screenImgRef.current.offsetLeft) / this.screenImgRef.current.clientWidth) * 800)
			y = Math.floor(((evt.pageY - this.screenImgRef.current.offsetTop) / this.screenImgRef.current.clientHeight) * 600)
		}

		setInterval(() => {
			if(x != null && y != null && (x !== lastX || y !== lastY))
				dispatchMouseMoveEvent()
		}, 250)

		const handleMouseButton = evtName => async evt => {
			dispatchEvent({
				op: evtName,
				args: {
					button: ["left", "middle", "right"][evt.button],
				},
			})
		}

		const handleKeyDown = async evt => {
			dispatchEvent({
				op: "keyDown",
				args: {
					code: evt.key,
				},
			})
			if(evt.key === "Tab") {
				this.screenImgRef.current.focus()
			}
			evt.preventDefault()
		}
		const handleKeyUp = async evt => {
			dispatchEvent({
				op: "keyUp",
				args: {
					code: evt.key,
				},
			})
			evt.preventDefault()
		}

		const handleWheel = async evt => {
			dispatchEvent({
				op: "scroll",
				args: {
					deltaX: evt.deltaX,
					deltaY: evt.deltaY,
				},
			})
		}


		return (
			<div className="browser-screen grow">
				<img className="browser-screen-img noselect"
					src={`data:image/png;base64,${screenshot}`}
					ref={this.screenImgRef}
					alt="Browser Screen"
					tabIndex={tabIndex}
					onMouseMove={handleMouseMove}
					onMouseDown={handleMouseButton("mouseDown")}
					onMouseUp={handleMouseButton("mouseUp")}
					onKeyDown={handleKeyDown}
					onKeyUp={handleKeyUp}
					onWheel={handleWheel} />
			</div>
		)
	}
}

export default hot(BrowserScreen)
