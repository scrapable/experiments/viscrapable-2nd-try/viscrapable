import React, { Component } from "react"
import { hot } from "react-hot-loader/root"
import { observer, inject } from "mobx-react"
import { BrowserRouter, HashRouter, Switch, Route, Redirect } from "react-router-dom"
import { Split } from "./split/index.jsx"
import ScreenNavButtons from "./screen-nav-buttons/index.jsx"
import recordingScreen from "./screens/recordingScreen.jsx"
import outputsScreen from "./screens/outputsScreen.jsx"
import inputsScreen from "./screens/inputsScreen.jsx"
import deployScreen from "./screens/deployScreen.jsx"
import "./style.css"

// eslint-disable-next-line no-undef
const Router = _buildEnv.TARGET === "electron" ? HashRouter : BrowserRouter

const screens = [recordingScreen, outputsScreen, inputsScreen, deployScreen]

class App extends Component {
	constructor(props) {
		super(props)
	}

	render() {
		return (
			<Router>
				<Split>
					<Switch>
						{screens
							.filter(screen => screen.toolbar != null)
							.map(screen => (
								<Route key={screen.path}
									path={screen.path}
									render={() => (
										<div className="toolbar">
											<screen.toolbar />
											<div style={{ width: ".6rem" }}></div>
											<ScreenNavButtons
												previous={screen.previous}
												next={screen.next}
												nextLabel={screen.nextLabel} />
										</div>
									)}>
								</Route>
							))}
					</Switch>
					<Switch>
						<Route path="/" exact>
							<Redirect to="record" />
						</Route>
						{screens.map(screen => (
							<Route key={screen.path}
								path={screen.path}
								render={() => <>
									<screen.main />
								</>}>
							</Route>
						))}
					</Switch>
				</Split>
			</Router>
		)
	}
}

export default hot(inject("globalState")(observer(App)))
