import React from "react"
import { hot } from "react-hot-loader/root"
import ReactModal from "react-modal"
import "./style.css"

const Modal = ({
	isOpen,
	children,
	footer, rightAlignFooter = true,
	width, height,
	style: { content, overlay } = {}
}) => {
	return (
		<ReactModal isOpen={isOpen}
			style={{
				overlay: {
					position: "absolute",
					top: 0,
					left: 0,
					width: "100%",
					height: "100%",
					background: "rgba(0, 0, 0, .2)",
					...overlay,
				},
				content: {
					display: "flex",
					flexDirection: "column",
					width: width || "35rem",
					height: "auto",
					minHeight: height,
					position: "absolute",
					top: "40%",
					left: "50%",
					bottom: "unset",
					right: "unset",
					transform: "translate(-50%, -50%)",
					background: "var(--bg-shade-1)",
					border: "none",
					boxShadow: "0 0 10px rgba(0, 0, 0, .4)",
					...content,
				},
			}}
			overlayClassName="modal-overlay"
			portalClassName="modal-content">
			<div className="modal-body">
				{children}
			</div>
			<div className={`modal-footer ${rightAlignFooter ? "right-aligned" : ""}`}>
				{footer}
			</div>
		</ReactModal>
	)
}
 
export default hot(Modal)