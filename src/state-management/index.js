import { decorate, observable, action } from "mobx"
import { sleep } from "../utils"

class StateManager {
	constructor(browser) {
		this.browser = browser
		this.recordingState = "manual"
		this.eventQueue = []
		this.lastInstructionTimestamp = null

		this.browserLocation = ""
		if(this.browser) {
			this.browser.addEventListener("locationChanged", ({ newValue }) => {
				this.browserLocation = newValue
			})
		}

		this.lastScreenshot = ""
		if(this.browser) {
			setInterval(async () => {
				try {
					const screen = await browser.screenshot()
					this.lastScreenshot = screen
				// eslint-disable-next-line no-empty
				} catch(err) { }
			}, 125)
		}
	}

	async executeEvent(event) {
		if(this.browser)
			await this.browser.execute(event)
		else if(event.op === "goto") {
			await sleep(300)
			this.browserLocation = `${event.args.url}?redirect`
			await sleep(300)
			this.browserLocation = event.args.url
		}
		else if(event.op !== "mouseMove" && event.op !== "scroll")
			await sleep(100)
	}

	async setRecordingState(newState) {
		const oldState = this.recordingState
		this.recordingState = newState
		if(oldState !== newState) {
			if(newState === "playback") {
				await this.executeEvent({ op: "reset" })
				for (const event of this.eventQueue) {
					if(this.recordingState !== "playback") {
						break
					}
					await this.executeEvent(event)
				}
				await this.setRecordingState("manual")
			}
			else if(newState === "recording") {
				this.lastInstructionTimestamp = null
				this.clearEventQueue()
			}
		}
	}

	async dispatchEvent(event) {
		if(this.recordingState === "playback")
			return
		const isRecording = this.recordingState === "recording"
		event.attachMetadata = isRecording
		await this.executeEvent(event)
		delete event.attachMetadata
		if(isRecording) {
			if(event.delayBefore) {
				this.lastInstructionTimestamp += event.delayBefore
			}
			else {
				const now = Date.now()
				event.delayBefore = this.lastInstructionTimestamp ? (now - this.lastInstructionTimestamp) : 0
				this.lastInstructionTimestamp = now
			}
			this.mergeEventIntoQueue(event)
		}
	}

	mergeEventIntoQueue(event) {
		if(!event.metadata) {
			this.pushEventIntoQueue(event)
			return
		}

		const focused = event.metadata.focusedElementInfo

		let lastIndex
		let lastEvent
		const refreshLast = () => {
			lastIndex = this.eventQueue.length - 1
			lastEvent = this.eventQueue[lastIndex]
		}
		refreshLast()

		if(
			(event.op === "keyUp" || event.op === "keyDown") &&
            !(event.args.code === "Enter" || event.args.code === "Tab") &&
            focused && focused.tagName === "INPUT" &&
            (focused.type === null || focused.type === "text" || focused.type === "password")
		) {
			const text = focused.value
			if(lastEvent && lastEvent.op === "type" && lastEvent.args.selector === event.metadata.focusedSelector) {
				lastEvent.args.text = text
			} else {
				this.pushEventIntoQueue({
					op: "type",
					args: {
						text: focused.value,
						selector: event.metadata.focusedSelector,
					},
					delayBefore: event.delayBefore,
					metadata: event.metadata,
				})
			}
		} else if(
			focused && focused.tagName === "INPUT" && focused.type === "checkbox" &&
            (event.op === "keyUp" && event.args.code === " " || event.op === "mouseUp")
		) {
			if(lastEvent.metadata.focusedSelector === event.metadata.focusedSelector && (
				event.op === "keyUp" && lastEvent.op === "keyDown" && lastEvent.args.code === " " ||
                event.op === "mouseUp" && lastEvent.op === "mouseDown"
			)) {
				this.removeEventFromQueue(lastIndex)
				refreshLast()
			}
			if(lastEvent.op === "check") {
				lastEvent.args.checked = focused.checked
			} else {
				this.pushEventIntoQueue({
					op: "check",
					args: {
						checked: focused.checked,
						selector: event.metadata.focusedSelector,
					},
					delayBefore: event.delayBefore,
					metadata: event.metadata,
				})
			}
		} else {
			this.pushEventIntoQueue(event)
		}
	}

	pushEventIntoQueue(event) {
		this.eventQueue.push(event)
	}

	removeEventFromQueue(index) {
		this.eventQueue.splice(index)
	}

	clearEventQueue() {
		this.eventQueue = []
	}
}
	
decorate(StateManager, {
	browserLocation: observable,
	lastScreenshot: observable,
	recordingState: observable,
	eventQueue: observable,
	setRecordingState: action.bound,
	dispatchEvent: action.bound,
	removeEventFromQueue: action.bound,
	clearEventQueue: action.bound,
})

export default StateManager
