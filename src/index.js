const { app, BrowserWindow, Menu } = require("electron");

(async () => {
	let mainWindow
	const createMainWindow = () => {
		mainWindow = new BrowserWindow({
			width: 1200,
			height: 800,
			webPreferences: {
				nodeIntegration: true,
			},
		})

		// Hack to avoid having a menu that could possibly react to a keypress
		mainWindow.setMenu(new Menu())
		mainWindow.setMenuBarVisibility(false)

		mainWindow.on("closed", () => {
			mainWindow = null
		})
		mainWindow.loadFile("index.html")
	}

	app.on("ready", createMainWindow)

	app.on("window-all-closed", async () => {
		if(process.platform !== "darwin") app.quit()
	})

	app.on("activate", () => {
		if(mainWindow == null) createMainWindow()
	})
})()
