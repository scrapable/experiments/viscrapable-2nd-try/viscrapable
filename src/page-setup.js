const React = require("react")
const ReactDOM = require("react-dom")
const { Provider } = require("mobx-react")
const App = require("./ui").default
const StateManager = require("./state-management").default

export const setUpApp = browser => {
	const stateManager = new StateManager(browser)

	const dispatchEvent = async (event) => {
		await stateManager.dispatchEvent(event)
	}

	ReactDOM.render((
		<Provider globalState={stateManager}>
			<App onBrowserEvent={dispatchEvent} />
		</Provider>
	), document.body)

	return stateManager
}

export const setUpDevEnvIfRequested = async stateManager => {
	if(process.env.NODE_ENV === "dev" || process.env.NODE_ENV === "development") {
		await stateManager.setRecordingState("recording")
		await stateManager.dispatchEvent({op: "goto", args: {url: "https://news.ycombinator.com"}})
		await stateManager.dispatchEvent({op: "export", args: {
			selector: ".title a",
			prop: "href",
			name: "firstLink",
		}})
	}
}
