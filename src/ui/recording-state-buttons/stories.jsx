import React, { useState } from "react"
import RecordingStateButtons from "./index.jsx"
import "../style.css"

export default {
	title: "RecordingStateButtons",
}

export const recording = () => (
	<div className="toolbar">
		<RecordingStateButtons recordingState="recording" onStateChanged={() => { }} />
	</div>
)

export const playback = () => (
	<div className="toolbar">
		<RecordingStateButtons recordingState="playback" onStateChanged={() => { }} />
	</div>
)

export const manual = () => (
	<div className="toolbar">
		<RecordingStateButtons recordingState="manual" onStateChanged={() => { }} />
	</div>
)

export const switchable = () => {
	const [ recState, setRecState ] = useState("manual")

	return (
		<div>
			<div className="toolbar">
				<RecordingStateButtons recordingState={recState} onStateChanged={setRecState} />
			</div>
			<div style={{ margin: "0 1rem" }}>State: {recState}</div>
		</div>
	)
}
