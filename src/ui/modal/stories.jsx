import React, { useState } from "react"
import ReactModal from "react-modal"
import Modal from "./index.jsx"
import "../style.css"

ReactModal.setAppElement(document.body)

export default {
	title: "Modal",
}

export const simple = () => {
	const [ isOpen, setOpen ] = useState(true)
	return (
		<div>
			<button onClick={() => setOpen(true)}
				style={{ color: "white" }}>
				Open
			</button>
			<Modal isOpen={isOpen} width="25rem" height="15rem">
				<p>This is a very simple modal.</p>
				<button onClick={() => setOpen(false)}
					style={{ color: "white" }}>
					Close
				</button>
			</Modal>
		</div>
	)
}

export const withFooter = () => {
	const [ isOpen, setOpen ] = useState(true)
	return (
		<div>
			<button onClick={() => setOpen(true)}
				style={{ color: "white" }}>
				Open
			</button>
			<Modal isOpen={isOpen}
				width="25rem"
				height="15rem"
				footer={(
					<button onClick={() => setOpen(false)}
						style={{ color: "white" }}>
						Close
					</button>
				)}>
				<h1>Footer</h1>
				<p>This is a modal with a footer.</p>
			</Modal>
		</div>
	)
}

export const withFooterLeftAligned = () => {
	const [ isOpen, setOpen ] = useState(true)
	return (
		<div>
			<button onClick={() => setOpen(true)}
				style={{ color: "white" }}>
				Open
			</button>
			<Modal isOpen={isOpen}
				width="30rem"
				height="15rem"
				footer={(
					<button onClick={() => setOpen(false)}
						style={{ color: "white" }}>
						Close
					</button>
				)}
				rightAlignFooter={false}>
				<h1>Left-aligned Footer</h1>
				<p>This is a modal with a left-aligned footer.</p>
				<p>Footers are right-aligned per default.</p>
			</Modal>
		</div>
	)
}
