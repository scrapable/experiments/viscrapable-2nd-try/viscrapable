import React from "react"
import { hot } from "react-hot-loader/root"
import { Link as RRLink } from "react-router-dom"
import "./style.css"

const ScreenNavButtons = ({ previous, next, nextLabel = "Next", previousLabel, Link = RRLink }) => {
	return <div className="screen-nav-buttons">
		{previous && (
			<Link to={previous}
				className={`button screen-nav-button previous ${previousLabel ? "has-prev-label" : ""}`}>
				{previousLabel}
			</Link>
		)}
		{next && (
			<Link to={next}
				className="button screen-nav-button next">
				{nextLabel}
			</Link>
		)}
	</div>
}

export default hot(ScreenNavButtons)
