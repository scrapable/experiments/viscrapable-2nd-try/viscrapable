import React, { Component } from "react"
import { hot } from "react-hot-loader/root"
import "./style.css"

let listener

const stopListening = () => {
	listener = null
	document.body.classList.remove("noselect")
}

document.body.addEventListener("mousemove", evt => {
	if(listener) listener(evt)
})
document.body.addEventListener("mouseup", stopListening)

class S extends Component {
	constructor(props) {
		super(props)
		this.domRef = React.createRef()
	}

	render() {
		const {
			children,
			horizontal,
		} = this.props

		return (
			<div className={`split ${horizontal ? "horizontal" : "vertical"}`}
				ref={this.domRef}>
				{React.Children.map(children, c => {
					if(c != null)
						return React.cloneElement(c, {
							splitParentHorizontal: horizontal,
						})
				})}
			</div>
		)
	}
}

class R extends Component {
	constructor(props) {
		super(props)
		this.isSplitResizable = true
		this.domRef = React.createRef()
		this.state = {
			size: props.startSize,
		}
	}

	render() {
		const {
			splitParentHorizontal: horizontal,
			invert,
			style,
			children,
		} = this.props

		const startListening = evt => {
			const startPos = evt[horizontal ? "pageX" : "pageY"]
			const startSize = this.domRef.current[horizontal ? "clientWidth" : "clientHeight"]

			document.body.classList.add("noselect")

			listener = (function (moveEvt) {
				if(moveEvt.buttons !== 1) {
					stopListening()
					return
				}

				const pos = moveEvt[horizontal ? "pageX" : "pageY"]

				let newSize = startSize + (pos - startPos) * (horizontal ? 1 : -1) * (invert ? -1 : 1)
				newSize = Math.max(newSize, 20)
				this.setState({
					size: newSize,
				})
			}).bind(this)
		}

		return (
			<div className={`resizable ${invert && "invert"}`}
				style={{
					...style,
					[horizontal ? "width" : "height"]: this.state.size,
					[horizontal ? "minWidth" : "minHeight"]: this.state.size,
				}}
				ref={this.domRef}>
				{invert && children}
				<div className="resize-dragger"
					onMouseDown={startListening}>
				</div>
				{!invert && children}
			</div>
		)
	}
}


export const Split = hot(S)
export const Resizable = hot(R)
