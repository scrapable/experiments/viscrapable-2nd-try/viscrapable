import React from "react"
import { hot } from "react-hot-loader/root"


const OutputsScreenToolbar = () => (
	<div style={{ flexGrow: 1, lineHeight: "2rem" }}>Outputs</div>
)

const OutputsScreenMain = () => (
	<div style={{ backgroundColor: "var(--bg-shade-1)", flexGrow: 1, position: "relative" }}>
		<div style={{
			position: "absolute",
			top: "50%",
			left: "50%",
			transform: "translate(-50%, -50%)",
			opacity: .5,
		}}>Not implemented yet</div>
	</div>
)

const outputsScreen = {
	path: "/outputs",
	previous: "/record",
	next: "/inputs",
	toolbar: hot(OutputsScreenToolbar),
	main: hot(OutputsScreenMain),
}

export default outputsScreen
