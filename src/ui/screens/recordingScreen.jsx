import React from "react"
import { hot } from "react-hot-loader/root"
import { observer, inject } from "mobx-react"
import { Split, Resizable } from "../split/index.jsx"
import EventQueue from "../event-queue/index.jsx"
import BrowserBar from "../browser-bar/index.jsx"
import RecordingStateButtons from "../recording-state-buttons/index.jsx"
import BrowserScreen from "../browser-screen/index.jsx"

const RecordingScreenToolbar = props => {
	const {
		recordingState,
		setRecordingState,
		dispatchEvent,
		browserLocation,
	} = props.globalState

	return <>
		{/* <button onClick={() => this.setState({ side: (this.state.side + 1) % 4 })}>Toggle</button> */}
		<BrowserBar
			key={browserLocation}
			location={browserLocation}
			onEvent={dispatchEvent} />
		<div style={{width: ".5rem"}}></div>
		<RecordingStateButtons recordingState={recordingState}
			onStateChanged={setRecordingState} />
	</>
}

const RecordingScreenMain = props => {
	const {
		dispatchEvent,
		eventQueue,
		lastScreenshot,
	} = props.globalState

	const side = 1

	return (
		<Split horizontal={side % 2 === 1}>
			{side < 2 && (
				<Resizable startSize="30em" invert={side % 2 === 0} style={{ display: "flex", flexDirection: "column", background: "var(--bg-shade-3)" }}>
					<div style={{ overflowY: "auto", overflowX: "hidden", flexGrow: 1 }}>
						<EventQueue queue={eventQueue} />
					</div>
				</Resizable>
			)}
			<BrowserScreen screenshot={lastScreenshot} onEvent={dispatchEvent} tabIndex={0} />
			{side >= 2 && (
				<Resizable startSize="30em" invert={side % 2 === 1} style={{ display: "flex", flexDirection: "column", background: "var(--bg-shade-3)" }}>
					<div style={{ overflowY: "auto", overflowX: "hidden" }}>
						<EventQueue queue={eventQueue} />
					</div>
				</Resizable>
			)}
		</Split>
	)
}

const recordingScreen = {
	path: "/record",
	next: "/outputs",
	toolbar: hot(inject("globalState")(observer(RecordingScreenToolbar))),
	main: hot(inject("globalState")(observer(RecordingScreenMain))),
}

export default recordingScreen
