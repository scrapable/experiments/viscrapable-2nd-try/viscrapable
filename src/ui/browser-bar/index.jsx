import React, { Component } from "react"
import { hot } from "react-hot-loader/root"
import "./style.css"

import backIcon from "../icons/2B05.svg"
import forwardIcon from "../icons/27A1.svg"
import reloadIcon from "../icons/1F504.svg"


class BrowserBar extends Component {
	constructor(props) {
		super(props)
		this.state = {
			addressBarValue: props.location,
		}
	}

	render() {
		const {
			onEvent,
		} = this.props

		const dispatchEvent = async event => {
			if(onEvent)
				await onEvent(event)
		}

		const onAddressBarChange = evt => {
			this.setState({ addressBarValue: evt.target.value })
		}

		const onAddressBarSubmit = async evt => {
			evt.preventDefault()
			await dispatchEvent({ op: "goto", args: { url: this.state.addressBarValue } })
		}

		return (
			<div className="browser-bar">
				<button className="back"
					title="Back"
					onClick={() => dispatchEvent({ op: "navigateBack" })}>
					<img src={backIcon} alt="Back" />
				</button>
				<button className="forward"
					title="Forward"
					onClick={() => dispatchEvent({ op: "navigateForward" })}>
					<img src={forwardIcon} alt="Forward" />
				</button>
				<button className="reload"
					title="Reload / Reset (Cntrl+Alt)"
					onClick={evt => dispatchEvent({ op: evt.ctrlKey && evt.altKey ? "reset" : "reload" })}>
					<img src={reloadIcon} alt="Reload / Reset (Cntrl+Alt)" />
				</button>
				<form onSubmit={onAddressBarSubmit}>
					<input className="address-bar"
						type="text"
						value={this.state.addressBarValue}
						onChange={onAddressBarChange} />
				</form>
			</div>
		)
	}
}

export default hot(BrowserBar)
