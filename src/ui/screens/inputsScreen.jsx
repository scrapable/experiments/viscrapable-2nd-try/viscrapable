import React from "react"
import { hot } from "react-hot-loader/root"


const InputsScreenToolbar = () => (
	<div style={{ flexGrow: 1, lineHeight: "2rem" }}>Inputs</div>
)

const InputsScreenMain = () => (
	<div style={{ backgroundColor: "var(--bg-shade-1)", flexGrow: 1, position: "relative" }}>
		<div style={{
			position: "absolute",
			top: "50%",
			left: "50%",
			transform: "translate(-50%, -50%)",
			opacity: .5,
		}}>Not implemented yet</div>
	</div>
)

const inputsScreen = {
	path: "/inputs",
	previous: "/outputs",
	next: "/deploy",
	nextLabel: "Finish",
	toolbar: hot(InputsScreenToolbar),
	main: hot(InputsScreenMain),
}

export default inputsScreen
