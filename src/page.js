const { browser: b } = require("@scrpbl/engine")
const { remote } = require("electron")
const { autorun } = require("mobx")
const { setUpApp, setUpDevEnvIfRequested } = require("./page-setup");

(async () => {
	document.body.addEventListener("keydown", evt => {
		if(evt.key === "F12")
			remote.getCurrentWebContents().toggleDevTools()
	})

	const browser = new b.BrowserEngine()

	const stateManager = setUpApp(browser)
	

	// Set up browser

	await browser.launch({
		headless: true,
		executablePath: process.env.VISCRAPABLE_CHROMIUM,
	})

	remote.getCurrentWindow().on("close", async () => await browser.close())

	autorun(() => {
		browser.introduceExtraDelay = stateManager.recordingState === "playback"
	})


	await setUpDevEnvIfRequested(stateManager)
})()
