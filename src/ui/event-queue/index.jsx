import React, { Component } from "react"
import { hot } from "react-hot-loader/root"
import { observer } from "mobx-react"
import "./style.css"

const EventArg = ({ name, value }) => (
	<li className="arg">
		{name}: <span className={typeof(value)}>
			{value.toString().replace(/^(\s+)$/, "\"$1\"")}
		</span>
	</li>
)

class EventQueue extends Component {
	constructor(props) {
		super(props)
		this.lastEventRef = React.createRef()
	}

	render() {
		const { queue } = this.props

		const renderArgs = args => (
			Object.getOwnPropertyNames(args).map(name =>
				<EventArg name={name} value={args[name]} key={name} />
			)
		)

		return (
			<table className="event-queue">
				<thead>
					<tr>
						<th>Operation</th>
						<th>Arguments</th>
						<th>Delay</th>
					</tr>
				</thead>
				<tbody>
					{queue.map((event, i) => (
						<tr className="event"
							key={i}
							ref={this.lastEventRef}>
							<td className="op">{event.op}</td>
							<td className="args">
								<ul>
									{renderArgs(event.args)}
								</ul>
							</td>
							<td className="delay">{event.delayBefore}ms</td>
						</tr>
					))}
				</tbody>
			</table>
		)
	}

	componentDidUpdate() {
		if(this.lastEventRef && this.lastEventRef.current)
			this.lastEventRef.current.scrollIntoView()
	}
}
 
export default hot(observer(EventQueue))
