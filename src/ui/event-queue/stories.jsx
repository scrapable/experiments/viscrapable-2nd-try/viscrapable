import React from "react"
import EventQueue from "./index.jsx"
import exampleQueue from "./drag_n_drop.json"
import "../style.css"

export default {
	title: "EventQueue",
}

export const dragNDrop = () => (
	<EventQueue queue={exampleQueue} />
)
