import React from "react"
import { hot } from "react-hot-loader/root"
import { observer } from "mobx-react-lite"
import "./style.css"

import recordIcon from "../icons/record.svg"
import playIcon from "../icons/play.svg"

const RecordingStateButtons = ({ recordingState, onStateChanged }) => {
	const isRecordActive = () => recordingState === "recording"
	const isPlayActive = () => recordingState === "playback"

	return (
		<div className="recording-state-buttons">
			<button className={`record ${isRecordActive() ? "toggle-active" : ""}`}
				onClick={() => onStateChanged(recordingState === "recording" ? "manual" : "recording")}>
				<img src={recordIcon} alt="Record" />
			</button>
			<button className={`play ${isPlayActive() ? "toggle-active" : ""}`}
				onClick={() => onStateChanged(recordingState === "playback" ? "manual" : "playback")}>
				<img src={playIcon} alt="Play" />
			</button>
		</div>
	)
}

export default hot(observer(RecordingStateButtons))
