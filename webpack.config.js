const path = require("path")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const { DefinePlugin } = require("webpack")

const electronMain = {
	entry: "./src/index.js",
	output: {
		path: path.resolve(__dirname, "dist"),
		filename: "main.js",
	},
	target: "electron-main",
	module: {
		rules: [
		],
	},
}

const electronRenderer = {
	entry: "./src/page.js",
	output: {
		path: path.resolve(__dirname, "dist"),
		filename: "page.js",
	},
	target: "electron-main",  // Normally, this would be electron-renderer but that breaks puppeteer
	module: {
		rules: [
			{
				test: /\.jsx?$/,
				use: ["babel-loader"],
			},
			{
				test: /\.css$/,
				use: [
					"style-loader",
					"css-loader",
				],
			},
			{
				test: /\.svg$/,
				loader: "file-loader",
			},
		],
	},
	plugins: [
		new HtmlWebpackPlugin(),
		new DefinePlugin({
			"_buildEnv.TARGET": "'electron'",
		}),
	],
}

module.exports = [electronMain, electronRenderer]
