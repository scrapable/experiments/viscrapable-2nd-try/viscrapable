const path = require("path")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const { DefinePlugin } = require("webpack")

const distPath = path.resolve(__dirname, "dist")

module.exports = {
	entry: ["react-hot-loader/patch", "./src/page-dev-server.js"],
	output: {
		path: distPath,
		filename: "page.js",
	},
	target: "web",
	devServer: {
		contentBase: distPath,
		port: 9000,
		hot: true,
		historyApiFallback: true,
	},
	module: {
		rules: [
			{
				test: /\.jsx?$/,
				use: ["babel-loader"],
			},
			{
				test: /\.css$/,
				use: [
					"style-loader",
					"css-loader",
				],
			},
			{
				test: /\.svg$/,
				loader: "file-loader",
			},
		],
	},
	plugins: [
		new HtmlWebpackPlugin(),
		new DefinePlugin({
			"_buildEnv.TARGET": "'web'",
		}),
	],
}
