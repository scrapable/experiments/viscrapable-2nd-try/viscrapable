import React from "react"
import { hot } from "react-hot-loader/root"
import ScreenNavButtons from "../screen-nav-buttons/index.jsx"

const DeployScreenMain = () => (
	<div style={{ backgroundColor: "var(--bg-shade-1)", flexGrow: 1, position: "relative" }}>
		<div style={{
			position: "absolute",
			top: "40%",
			left: "50%",
			transform: "translate(-50%, -50%)",
			textAlign: "center",
		}}>
			<h1>All done!</h1>
			<div style={{ opacity: .5 }}>
				<p>Your first Scraper is finished!</p>
				<p>Here you could deploy, however that's not implemented yet.</p>
			</div>
			<ScreenNavButtons previous="/inputs" previousLabel="Back" />
		</div>
	</div>
)

const deployScreen = {
	path: "/deploy",
	main: hot(DeployScreenMain),
}

export default deployScreen
